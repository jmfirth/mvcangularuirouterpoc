# MvcAngularUiRouterPoC

A proof of concept using ASP.NET MVC for bundling, route redirection, and environment-specific configuration of an Angular SPA using Angular UI Router.

## Demonstrates

###via ASP.NET MVC
- Route redirection for SPAs
- CSS bundling
- JS bundling
- Angular template bundling (for $templateCache)
- Environment-specific configuration
- Error route handling

###via Angular
- Angular module creation
- Angular UI Router configuration
- Custom ConfigProvider
- Abstract templates
- Controllers
- Directives

## Running

Open solution and debug.  
