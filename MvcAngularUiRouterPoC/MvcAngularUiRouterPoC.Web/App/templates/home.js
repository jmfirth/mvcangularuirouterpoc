﻿(function () {
    'use strict';

    function HomeCtrl($scope, $state, $rootScope, Config) {
        $scope.message = 'Hello from HomeCtrl';
        $scope.exampleMessage = 'Hello from HomeCtrl (via Example directive)';
        $scope.logLevel = Config.logLevel;
    }

    HomeCtrl.$inject = ['$scope', '$state', '$rootScope', 'Config'];

    angular
        .module('app')
        .controller('HomeCtrl', HomeCtrl);

})();