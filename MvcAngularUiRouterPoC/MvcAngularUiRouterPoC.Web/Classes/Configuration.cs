﻿using System.Web.Configuration;

namespace MvcAngularUiRouterPoC.Web.Classes
{
    /// <summary>
    /// Configuration helper class
    /// </summary>
    public static class Configuration
    {
        /// <summary>
        /// The front end log level
        /// </summary>
        public static string LogLevel
        {
            get { return _logLevel ?? (_logLevel = WebConfigurationManager.AppSettings["LogLevel"]); }
        }
        private static string _logLevel = null;
        
        /// <summary>
        /// Enable bundling defined in /AppStart/BundleConfig.cs
        /// </summary>
        public static bool EnableBundling
        {
            get { return _enableBundling.HasValue ? _enableBundling.Value : (_enableBundling = bool.Parse(WebConfigurationManager.AppSettings["EnableBundling"])).Value; }
        }
        private static bool? _enableBundling = null;
    }
}