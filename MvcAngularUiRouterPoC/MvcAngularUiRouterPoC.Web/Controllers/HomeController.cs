﻿using System.Web.Mvc;
using MvcAngularUiRouterPoC.Web.Classes;

namespace MvcAngularUiRouterPoC.Web.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.LogLevel = Configuration.LogLevel;
            return View();
        }
    }
}